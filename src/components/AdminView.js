import {Form, Modal, Table, Container, Row, Col, Card, Button} from 'react-bootstrap';
import {Fragment, useEffect, useState} from 'react'
import Swal from "sweetalert2";


export default function AdminView(props){


	const {courseData, fetchData} = props;
	const [name, setName] = useState("");
	const [description, setDescription] = useState("");
	const [price, setPrice] = useState(0);
	const [courseId, setCourseId] = useState("")

	const [courses, setCourses] = useState([]);

	// add course button state
	const [showAdd, setShowAdd] = useState(false);

	//function to open & close button
	const openAdd = () => setShowAdd(true);
    const closeAdd = () => setShowAdd(false);



	useEffect( () => {
		const coursesArr = courseData.map( course => {
			return(
				/*console.log(course)*/
				<tr>
					<td>{course.name}</td>
					<td>{course.description}</td>
					<td>{course.price}</td>
					<td>
					{
						(course.isActive === true) ?
							<span>Available</span>
						:
							<span>Not Available</span>
					}
					</td>
					<td>
						<Button onClick={() => openEdit(course._id)}>Update</Button>
						{
							(course.isActive) ?
								<Fragment>
									<Button 
										variant="danger"
										onClick={ () => archiveCourse(course._id, course.isActive)}
										>Disable</Button>
									<Button variant="secondary">Delete</Button>
								</Fragment>
							:
							<Fragment>
								<Button variant="success">Enable</Button>
								<Button variant="secondary">Delete</Button>
							</Fragment>
						}
					</td>
				</tr>
			)
		}) 	
			setCourses(coursesArr)
		}, [courseData])

	//add course function to be invoked when onSubmit event takes place(see add course modal code)
	const addCourse = (e) => {
		e.preventDefault()
		fetch("https://orig-capstone.herokuapp.com/api/products/",{
			method: "POST",
			headers:{
				"Content-Type": "application/json",
				"Authorization": `Bearer ${localStorage.getItem("token")}`
			},
			body: JSON.stringify({
				courseName: name,
				description: description,
				price: price
			})
		})
		.then(res => res.json())
		.then(data =>{
			console.log(data)
			if(data === true){
				Swal.fire({
				    title:'Success!',
				    icon: 'success',
				    text: 'Successfully Added!'
				})
				//setting back to original state after successfully added the course
				setName("")
				setPrice(0)
				setDescription("")
				//close the modal after the alert
				closeAdd();
				//function passed from courses page component to retrive all courses
				fetchData()

			} else {
				Swal.fire({
				    title:'Error!',
				    icon: 'error',
				    text: 'Please try again.'
				})
				fetchData()
			}
		})
	}

	//function to populate data in the form upon clicking Update button
	const openEdit = (courseId) => {

		fetch(`http://localhost:4000/api/products/${courseId}`, {
			headers: {
				"Authorization": `Bearer ${localStorage.getItem("token")}`
			}
		})
		.then(response => response.json())
		.then(data => {
			console.log(data)

			setName(data.name)
			setDescription(data.description)
			setPrice(data.price)
		})

		setShowAdd(true)
	}

	const editCourse = (e, courseId) => {
		e.preventDefault()
		fetch(`https://orig-capstone.herokuapp.com/api/products/${courseId}/productId`,{
			method: "PUT",
			headers:{
				"Content-Type": "application/json",
				"Authorization": `Bearer ${localStorage.getItem("token")}`
			},
			body: JSON.stringify({
				courseName: name,
				description: description,
				price: price
			})
		})
		.then(res => res.json())
		.then(data =>{
			console.log(data)
			if(typeof data !== "undefined"){
				fetchData()

				Swal.fire({
				    title:'Success!',
				    icon: 'success',
				    text: 'Successfully Added!'
				})
				closeAdd()

			} else {
				Swal.fire({
				    title:'Error!',
				    icon: 'error',
				    text: 'Please try again.'
				})
				fetchData()
			}
		})
	}

	//archive course
	const archiveCourse = (courseId, isActive) => {
		console.log(isActive)
		fetch(`https://orig-capstone.herokuapp.com/api/products/${courseId}/archive`, {
			method: "PUT",
			headers: {
				"Content-Type": "application/json",
				"Authorization": `Bearer ${localStorage.getItem("token")}`
			},
			body: JSON.stringify({
				isActive: !isActive
			})
		})
		.then(response => response.json())
		.then(data => {
			console.log(data)

			if(data.isActive === false){

				fetchData()

				Swal.fire({
					title: "Success",
					icon: "success",
					text: "Course disabled"
				})
			} else {
				fetchData()

				Swal.fire({
					title: "Something went wrong",
					icon: "error",
					text: "Please try again."
				})
			}
		})
	}

	return(
		<Container>
			<h2 className="text-center">Admin Dashboard</h2>
			<Row className="justify-content-center">
				<Col>
					<div className ="text-right">
						<Button onClick ={openAdd}>Add New Course</Button>
					</div>
				</Col>
			</Row>
			<Table striped bordered hover variant="dark">
			  <thead>
			    <tr>
			      <th>Name</th>
			      <th>Description</th>
			      <th>Price</th>
			      <th>Availablity</th>
			      <th>Actions</th>
			    </tr>
			  </thead>
			  <tbody>
			   	{courses}
			  </tbody>
			</Table>
		{/*Add Course Modal*/}
		<Modal show={showAdd} onHide={closeAdd}>
			<Modal.Header closeButton>
				<Modal.Title>Add Course</Modal.Title>
			</Modal.Header>
			<Modal.Body>
				<Form onSubmit={ (e) => addCourse(e)}>
					<Form.Group controlId="name">
						<Form.Label>Course Name:</Form.Label>
						<Form.Control 
							type="text"
							value={name}
							onChange={ (e) => setName(e.target.value)}/>
					</Form.Group>
					<Form.Group controlId="description">
						<Form.Label>Description:</Form.Label>
						<Form.Control 
							type="text" 
							value={description}
							onChange={ (e) => setDescription(e.target.value)}/>
					</Form.Group>
					<Form.Group controlId="price">
						<Form.Label>Price:</Form.Label>
						<Form.Control 
							type="number" 
							value={price}
							onChange={ (e) => setPrice(e.target.value)}/>
					</Form.Group>
					<Button variant="success" type="submit">
					    Submit
					</Button>
					<Button 
						variant="secondary" 
						type="submit" 
						onClick={closeAdd}>
					    Close
					</Button>
				</Form>
			</Modal.Body>
		</Modal>

		{/*Edit Course*/}
		<Modal show={showAdd} onHide={closeAdd}>
			<Modal.Header closeButton>
				<Modal.Title>Edit Course</Modal.Title>
			</Modal.Header>
			<Modal.Body>
				<Form onSubmit={ (e) => editCourse(e, courseId)}>
					<Form.Group controlId="name">
						<Form.Label>Course Name:</Form.Label>
						<Form.Control 
							type="text"
							value={name}
							onChange={ (e) => setName(e.target.value)}/>
					</Form.Group>
					<Form.Group controlId="description">
						<Form.Label>Description:</Form.Label>
						<Form.Control 
							type="text" 
							value={description}
							onChange={ (e) => setDescription(e.target.value)}/>
					</Form.Group>
					<Form.Group controlId="price">
						<Form.Label>Price:</Form.Label>
						<Form.Control 
							type="number" 
							value={price}
							onChange={ (e) => setPrice(e.target.value)}/>
					</Form.Group>
					<Button variant="success" type="submit">
					    Submit
					</Button>
					<Button 
						variant="secondary" 
						type="submit" 
						onClick={closeAdd}>
					    Close
					</Button>
				</Form>
			</Modal.Body>
		</Modal>

		</Container>

	)

}
