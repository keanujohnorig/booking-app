import {useState, useEffect} from 'react';
import {Container, Row, Col, Card, Button} from 'react-bootstrap';
import {Link} from 'react-router-dom';

export default function CourseCard({courseProp}){
	// console.log(props)
	console.log(courseProp)


	/*object destructuring*/
	const {name, description, price, isActive, _id} = courseProp

	const [isDisabled, setIsDisabled] = useState(false)

	return(
		<Container fluid>
			<Row className="mb-3">
				<Col xs={8} md={4}>
					<Card>
					  <Card.Body>
					    <Card.Title>{name}</Card.Title>
					    <Card.Subtitle>Description:</Card.Subtitle>
					    <Card.Text>
					      {description}
					    </Card.Text>
					    <Card.Subtitle>Price:</Card.Subtitle>
					    <Card.Text>
					      {price}
					    </Card.Text>
					    <Link className="btn btn-primary" to={`/courses/${_id}`}>Details</Link>

					  </Card.Body>
					</Card>
				</Col>
			</Row>
		</Container>
	)
}