import {useEffect, useState, Fragment} from 'react'
import CourseCard from './CourseCard';



export default function UserView({courseData}){

	const[courses, setCourses] = useState([]);
	console.log(courseData)
	useEffect( () => {
		const coursesArr = courseData.map( course => {
			if (course.isActive === true) {
				return(
					<CourseCard courseProp={course} key={course._id} />
				)
			} else{
				return null
			}	
		})
		setCourses(coursesArr);

	}, [courseData])
	console.log(courses)
	return(
		<Fragment>
			{courses}
		</Fragment>
	)


}
