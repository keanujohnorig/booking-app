
import {Container, Row, Col, Button} from 'react-bootstrap'
import {Link} from 'react-router-dom'

export default function Banner({data}){
	const {title, content, link, button} = data;

	return(
		<Container fluid>
			<Row>
				<Col>
					<div className="jumbotron">
						<h1>{title}</h1>
						<p>{content}</p>
						<Button variant="primary"><Link to={link} style={{color:'white'}}>{button}</Link></Button>
					</div>
				</Col>
			</Row>
		</Container>

	)
}