/*import {Fragment} from 'react'*/
import {useState, useEffect} from 'react'
import {Container} from 'react-bootstrap'
import 'bootstrap/dist/css/bootstrap.min.css'; 
import {
  BrowserRouter,
  Switch,
  Route
} from "react-router-dom";

/*for react context*/
import {UserProvider} from './UserContext';

/*components*/
import AppNavbar from './components/AppNavbar';

/*pages*/
import Home from './pages/Home';
import Courses from './pages/Courses';
import Register from './pages/Register';
import Login from './pages/Login';
import Logout from './pages/Logout';
import Error from './pages/Error';
import CourseView from './pages/CourseView';

export default function App() {

  const [user, setUser] = useState({

    id: null,
    isAdmin: null
  })

  const unsetUser = () => {
    localStorage.clear();

    setUser({
      id: null,
      isAdmin:null
    })

  }

  useEffect( () =>{
    fetch("https://orig-capstone.herokuapp.com/api/users/details",{
      headers:{
          "Authorization": `Bearer ${localStorage.getItem("token")}`
      }
    })
      .then( response => response.json())
      .then( data => {
        if(typeof data._id !== "undefined"){
          setUser({
            id: data._id,
            isAdmin: data.isAdmin
          })
        } 
      })  
    }, [])

  return(
    <UserProvider value = {{user, setUser, unsetUser}}>
      <BrowserRouter>
        <AppNavbar/>
          <Container fluid className= "m-3">
            <Switch>
                <Route exact path="/" component={Home}/>
                <Route exact path="/Courses" component={Courses}/>
                <Route exact path="/Courses/:courseId" component={CourseView}/>
                <Route exact path="/Register" component={Register}/>
                <Route exact path="/Logout" component={Logout}/>
                <Route exact path="/Login" component={Login}/>
                <Route component={Error} />
            </Switch>
          </Container>
      </BrowserRouter>
    </UserProvider>
    )
}

//https://orig-capstone.herokuapp.com/api/users