import {Fragment, useEffect, useState, useContext} from 'react'
/*import courseData from './../data/courseData';*/
import UserContext from './../UserContext'
/*components*/
	/*CourseCard is the template for courses*/
import CourseCard from './../components/CourseCard';
import AdminView from './../components/AdminView';
import UserView from './../components/UserView';

export default function Courses(){
	const[courses, setCourses] = useState([]);
	const {user} = useContext(UserContext)
	const fetchData = () =>{
		fetch("https://orig-capstone.herokuapp.com/api/products/all")
		.then(res => res.json())
		.then(data =>{
			//console.log(data)
			setCourses(data)
		})
	}
	/*console.log(courseData)*/	//array of objects

	/*const courses = courseData.map( element => {
		console.log(element)	//each object in the courseData array

		return(
			<CourseCard key={element.id} courseProp={element}/>
		)
	})*/
	useEffect(() => {
		fetchData()
		
	}, [])
	
	return (
		<Fragment>
			{	
				(user.isAdmin === true) ?
					<AdminView courseData = {courses} fetchData={fetchData}/>
				:
					<UserView courseData = {courses}/>
			}
			
		</Fragment>
			
	)
}