import {useState, useEffect, useContext} from 'react';
import {Button, Container, Row, Col, Card} from 'react-bootstrap';
import {useParams, useHistory, Link} from 'react-router-dom'
import Swal from "sweetalert2";
import UserContext from './../UserContext'

export default function CourseView(){
	const [name, setName] = useState("")
	const [description, setDescription] = useState("")
	const [price, setPrice] = useState("")
	let history = useHistory();
	const {user} = useContext(UserContext)

	const {courseId} = useParams()
	/*console.log(courseId) 61af31b2acc71adc10e6cc6c */
	useEffect(() => {
		fetch(`https://orig-capstone.herokuapp.com/api/products/${courseId}`)
		.then(res => res.json())
		.then(data =>{
			console.log(data)
			setName(data.name)
			setDescription(data.description)
			setPrice(data.price)
		})
	}, [courseId])

	const enroll = () =>{
		

		fetch(`https://orig-capstone.herokuapp.com/api/orders/checkout/${courseId}`,{
			method: "POST",
			headers: {
				"Content-Type": "application/json",
				"Authorization": `Bearer ${localStorage.getItem("token")}`
			},
			body: JSON.stringify({
				courseId:courseId
			})
		})
		.then(res => res.json())
		.then(data =>{
			console.log(data)
			if(data === true){
				Swal.fire({
				    title: 'Successful!',
				    icon: "success", 
				    text: 'Your transaction is Successful'
				})  
				history.push("/courses")
			} else{
				Swal.fire({
				    title: 'Something went wrong!',
				    icon: "error", 
				    text: 'Your transaction failed'
				})  

			}
		})


	}
	return(
		<Container>
			<Row className='justify-content-center'>
				<Col xs={8} md={4}>
					<Card>
						<Card.Body>
							<Card.Title>{name}</Card.Title>
							<Card.Subtitle>Description</Card.Subtitle>
							<Card.Text>{description}</Card.Text>
							<Card.Subtitle>Price</Card.Subtitle>
							<Card.Text>{price}</Card.Text>
							
							{
								(user.id !== null) ?
									<Button onClick={ () => enroll(courseId)}>Enroll</Button>
								:
									<Link className="btn btn-primary" to="/login">Login to enroll</Link>
							}
						</Card.Body>
					</Card>
				</Col>
			</Row>
		</Container>
	)
}